{
  config,
  lib,
  pkgs,
  ...
}:
let
  my-python-packages = p: with p; [ python-lsp-server ];
in
{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  imports = [
    ./theme.nix
    ./i3status-rs.nix
    ./helix.nix
    ./desktop.nix
  ];

  home.username = "not-leader";
  home.homeDirectory = "/home/not-leader";

  programs.bash = {
    enable = true;
    bashrcExtra = "${pkgs.pfetch}/bin/pfetch";
  };

  home.sessionVariables = {
    GTK_IM_MODULE = "fcitx";
    QT_IM_MODULE = "fcitx";
    XMODIFIERS = "@im=fcitx";
    INPUT_METHOD = "fcitx";
    GLFW_IM_MODULE = "ibus";
    SSH_AUTH_SOCK = "/run/user/1000/ssh-agent";
    EDITOR = "hx";
    SDL_VIDEODRIVER = "wayland";
  };

  xdg.configFile = {
    "wezterm/wezterm.lua".source = ./wezterm.lua;

    "sway/config".source = ./sway;
    "sway/grim-menu.sh" = {
      source = ./grim-menu.sh;
      executable = true;
    };

    "fuzzel/fuzzel.ini".source = ./fuzzel.ini;

    "git/allowed_signers".text = ''
      * ${builtins.readFile ./ssh-rsa.pub}
      * ${builtins.readFile ./ssh-ed25519.pub}
    '';
  };

  home.file = {
    ".ssh/id_rsa.pub".source = ./ssh-rsa.pub;
    ".ssh/id_ed25519.pub".source = ./ssh-ed25519.pub;
  };

  services = {
    gammastep = {
      enable = true;
      provider = "geoclue2";
      temperature.day = 6500;
    };

    ssh-agent.enable = true;

    mako.enable = true;

    udiskie.enable = true;
  };

  systemd.user.targets.tray = {
    Unit = {
      Description = "Home Manager System Tray";
      Requires = [ "graphical-session-pre.target" ];
    };
  };

  xsession.preferStatusNotifierItems = true;

  home.packages = with pkgs; [
    sway-contrib.grimshot
    pfetch
    hyfetch
    webcord-vencord
    xonotic-sdl
    ezquake
    dolphin-emu-primehack
    prismlauncher
    classicube
    minetest
    deluge-gtk
    fuzzel
    nil
    virt-manager
    axel
    keepassxc
    pulseaudio
    wl-clipboard
    qalculate-gtk
    free42
    (pkgs.python3.withPackages my-python-packages)
    xournalpp
    furnace
    sameboy
    nixfmt-rfc-style
  ];

  programs = {
    hexchat.enable = true;

    mpv.enable = true;

    git = {
      enable = true;
      userName = "not_leader";
      userEmail = "notleader1@outlook.com";
      extraConfig = {
        commit.gpgsign = true;
        gpg.format = "ssh";
        user.signingkey = "~/.ssh/id_rsa.pub";
        gpg.ssh.allowedSignersFile = "~/.config/git/allowed_signers";
      };
    };

    nheko.enable = true;

    librewolf.enable = true;

    wezterm.enable = true;
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
