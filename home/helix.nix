{
  config,
  pkgs,
  helix,
  ...
}:
{
  programs.helix = {
    enable = true;
    settings = {
      theme = "adwaita-dark";
      editor.line-number = "relative";
    };
    languages = {
      language = [
        {
          name = "nix";
          formatter.command = "nixfmt";
        }
      ];
    };
    package = helix.packages.${pkgs.system}.helix;
  };
}
