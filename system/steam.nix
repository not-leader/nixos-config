{
  config,
  lib,
  pkgs,
  ...
}:
{
  # List packages installed in system profile. To search, run:
  programs.steam = {
    enable = true;
    package = pkgs.steam.override {
      extraPkgs =
        pkgs: with pkgs; [
          cmake
          libogg
          libvorbis
          libtheora
          curl
          freetype
          libjpeg
          libpng
          SDL2
          libGL
          openal
          zlib
          libgcc
          xorg.libX11
          xorg.libXpm
          xorg.libXext
          xorg.libXxf86vm
          libGLU
          alsa-lib
        ];
    };
  };

  nixpkgs.config.allowUnfree = true;
}
